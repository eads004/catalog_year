#!/usr/bin/env python
import unittest
from catalog_year import DateVal, Rule, DATERULES, extract_year

class TestDateVal(unittest.TestCase):
    
    def test_constructor(self):
        d = DateVal("1855", "exact")
        self.assertEqual(d.orig, "1855")
        self.assertEqual(d.datetype, "exact")
        self.assertIsNone(d.earliest)
        self.assertIsNone(d.latest)
        

class TestRule(unittest.TestCase):
    
    def test_constructor(self):
        r = Rule(
            "rule_01", 
            r"^([0-9]{4})[.\]]*$", 
            "exact", 
            ["year"])
        self.assertEqual(r.fieldlist, ['year'])

class TestRuleList(unittest.TestCase):

    def test_there_are_rules(self):
        self.assertGreater(len(DATERULES), 0)

    def test_no_repeat_ids(self):
        ids = [r.ruleid for r in DATERULES]
        self.assertEqual(len(ids), len(set(ids)))

class TestExtract(unittest.TestCase):

    def test_simple_exact(self):
        v = extract_year("1855")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_uncertain_range(self):
        v = extract_year("[1855-1873?]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1855")
        self.assertEqual(v.latest, "1873")
        self.assertEqual(v.datetype, "range")

    def test_uncertain_range_ca(self):
        v = extract_year("[ca. 1625-1630?]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1625")
        self.assertEqual(v.latest, "1630")
        self.assertEqual(v.datetype, "range")

    def test_uncertain_range_2(self):
        v = extract_year("162[5-8?]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1625")
        self.assertEqual(v.latest, "1628")
        self.assertEqual(v.datetype, "range")

    def test_trailing_bracket(self):
        v = extract_year("1855]")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_brackets(self):
        v = extract_year("[1855]")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_brackets_punc(self):
        v = extract_year("[1855].")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_brackets_text(self):
        v = extract_year("Anno [1855].")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_brackets_alt(self):
        v = extract_year("1854 [1855].")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_ie(self):
        v = extract_year("1854 [i.e. 1855]")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_day_mo_year(self):
        v = extract_year("Jan. 15, 1855]")
        self.assertEqual(v.year, "1855")
        self.assertEqual(v.datetype, "exact")

    def test_between(self):
        v = extract_year("[between 1695 and 1700]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1695")
        self.assertEqual(v.latest, "1700")
        self.assertEqual(v.datetype, "range")

    def test_or(self):
        v = extract_year("1473 or 1474]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1473")
        self.assertEqual(v.latest, "1474")
        self.assertEqual(v.datetype, "range")

    def test_decade(self):
        v = extract_year("169-?]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1690")
        self.assertEqual(v.latest, "1699")
        self.assertEqual(v.datetype, "range")

    def test_match_optional_2digits_1(self):
        v = extract_year("[1684-95]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1684")
        self.assertEqual(v.latest, "1695")
        self.assertEqual(v.datetype, "range")

    def test_match_optional_2digits_2(self):
        v = extract_year("1680/81")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1680")
        self.assertEqual(v.latest, "1681")
        self.assertEqual(v.datetype, "range")

    def test_match_century_1(self):
        v = extract_year("[16-?]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1600")
        self.assertEqual(v.latest, "1699")
        self.assertEqual(v.datetype, "range")

    def test_match_century_2(self):
        v = extract_year("[16--?]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1600")
        self.assertEqual(v.latest, "1699")
        self.assertEqual(v.datetype, "range")

    def test_match_century_3(self):
        v = extract_year("[16--]")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1600")
        self.assertEqual(v.latest, "1699")
        self.assertEqual(v.datetype, "range")

    def test_slash(self):
        v = extract_year("1694/5.")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1694")
        self.assertEqual(v.latest, "1695")
        self.assertEqual(v.datetype, "range")

    def test_frac45(self):
        v = extract_year("169⁴/₅")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1694")
        self.assertEqual(v.latest, "1695")
        self.assertEqual(v.datetype, "range")

    def test_frac56(self):
        v = extract_year("169⁵/₆")
        self.assertIsNone(v.year)
        self.assertEqual(v.earliest, "1695")
        self.assertEqual(v.latest, "1696")
        self.assertEqual(v.datetype, "range")

    def test_uncertain(self):
        v = extract_year("1613?]")
        self.assertEqual(v.year, "1613")
        self.assertEqual(v.datetype, "uncertain")
        
    def test_uncertain2(self):
        v = extract_year("[1569?]")
        self.assertEqual(v.year, "1569")
        self.assertEqual(v.datetype, "uncertain")
        
    def test_circa(self):
        v = extract_year("[ca. 1600]")
        self.assertEqual(v.year, "1600")
        self.assertEqual(v.datetype, "circa")
        
    def test_match_uncertain_last_digit(self):
        v = extract_year("163[9?]")
        self.assertEqual(v.year, "1639")
        self.assertEqual(v.datetype, "uncertain")

    def test_match_sepdigits(self):
        v = extract_year("1569. February. 22.")
        self.assertEqual(v.year, "1569")
        self.assertEqual(v.datetype, "exact")

    def test_bracketed_1(self):
        v = extract_year("Iulij.5.1585 [5 July 1585]")
        self.assertEqual(v.year, "1585")
        self.assertEqual(v.datetype, "exact")

    def test_bracketed_2(self):
        v = extract_year("1586.Iulij.4 [4 July 1586]")
        self.assertEqual(v.year, "1586")
        self.assertEqual(v.datetype, "exact")

    def test_rule_x02(self):
        v = extract_year("An. M.D.XXXIII. [1533, i.e. 1534]")
        self.assertEqual(v.year, "1534")
        self.assertEqual(v.datetype, "exact")

    def test_rule_x03(self):
        v = extract_year("Feb. 20. 1696/7 [1697]]")
        self.assertEqual(v.year, "1697")
        self.assertEqual(v.datetype, "exact")

    def test_rule_x07(self):
        v = extract_year("1.5.5.0. [i.e. 1550]]")
        self.assertEqual(v.year, "1550")
        self.assertEqual(v.datetype, "exact")

    def test_roman_1(self):
        v = extract_year("M. D. L. ix.")
        self.assertEqual(v.year, "1559")
        self.assertEqual(v.datetype, "exact")

    def test_roman_2(self):
        v = extract_year("MDCXCI")
        self.assertEqual(v.year, "1691")
        self.assertEqual(v.datetype, "exact")

if __name__=="__main__":
    unittest.main()

