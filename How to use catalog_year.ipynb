{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to use `catalog_year`\n",
    "\n",
    "## Purpose\n",
    "\n",
    "The catalog_year library attempts to organize a miscellany of parsing rules to extract publication year information from EEBO-TCP metadata without losing information about uncertainty and spans of years.\n",
    "\n",
    "A given application may choose to simplify or discard information about uncertainty for a particular purpose, but another use of the TCP metadata might well want to focus on sites of uncertainty in order to seek out better information. In addition, quantitative analysis aimed at historical inference may need to keep track of such uncertainty for the sake of sensitivity analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic usage\n",
    "\n",
    "The primary function, `extract_year`, accepts a string and returns a custom date object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from catalog_year import extract_year"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = extract_year(\"1583\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Date object properties: \n",
      "\tdatetype, earliest, latest, orig, ruleid, year\n"
     ]
    }
   ],
   "source": [
    "print(\"Date object properties: \\n\\t{}\".format(\n",
    "    \", \".join([p for p in dir(d) if p[0] != '_'])))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `datetype` property identifies whether the year is an exact match, a range of possible years, or otherwise uncertain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'exact'"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d.datetype"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `orig` property keeps the original string, and if an exact year is parseable, it's stored in `year`.\n",
    "\n",
    "In the case of a range of years, `earliest` and `latest` are used.\n",
    "\n",
    "`ruleid` identifies the parsing rule that matched this date. This will most likely be useful only in debugging, testing, or extending the rules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('1583', None, None, '1583', 'match_bracketed')"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('exact', '[1587]', None, None, '1587', 'match_bracketed')"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"[1587]\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('range', '1604-1620', '1604', '1620', None, 'rule_r01')"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"1604-1620\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('range', '1604-20', '1604', '1620', None, 'match_optional_2digits')"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"1604-20\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('range', '180x', '1800', '1809', None, 'match_decade')"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"180x\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('range', '18--', '1800', '1899', None, 'match_century')"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"18--\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('range', '1604-20', '1604', '1620', None, 'match_optional_2digits')"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"1604-20\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('uncertain', '[1534?]', None, None, '1534', 'rule_u02')"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"[1534?]\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('exact', 'MDCXVIII', None, None, '1618', 'match_roman')"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = extract_year(\"MDCXVIII\")\n",
    "d.datetype, d.orig, d.earliest, d.latest, d.year, d.ruleid"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to inspect or modify the rules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each rule is a custom rule object, and all the rules are maintained in a Python list that can be imported as `DATERULES`.\n",
    "\n",
    "When `extract_year` is run, it tries each rule in the list in order and stops by applying the *first* rule that matches. When debugging or modifying the rules, it's possible for a rule to work as expected on its own, but not function as expected in the context of the pipeline of rules. There is undoubtedly room for improvement here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "from catalog_year import DATERULES"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "23"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(DATERULES)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "rule_ur01, rule_c01, rule_u01, rule_u02, rule_r01, rule_r02, match_optional_digit, match_optional_2digits, match_uncertain_last_digit, match_last_digit_frac45, match_last_digit_frac56, match_decade, match_century, match_uncertain_range_2, match_bracketed, match_sepdigits, rule_x01, rule_x02, rule_x03, rule_x04, rule_x05, match_roman, rule_n01\n"
     ]
    }
   ],
   "source": [
    "print(\", \".join((str(s) for s in DATERULES)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Date rule properties: \tfieldlist, matchfunc, pattern, rtype, ruleid\n"
     ]
    }
   ],
   "source": [
    "rule = DATERULES[0]\n",
    "print(\"Date rule properties: \\t{}\".format(\n",
    "    \", \".join([p for p in dir(rule) if p[0] != '_'])))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parts of a rule that determine its function are `pattern`, which specifies a regular expression pattern to match, including one or more capture groups, `matchfunc`, a function that specifies what to do with the capture groups, and `fieldlist`, which identifies fields for the rule to return when the parse matches. \n",
    "\n",
    "Each rule has a `ruleid` that is meant to identify it, which can help in debugging or testing the application of rules. The `rtype` attribute doesn't affect the application of a rule, but identifies the kind of dates it will return as `exact`, `range`, or `uncertain`.\n",
    "\n",
    "There is a default `matchfunc` function that simply maps capture groups to fields in the fieldlist, in order. This is often (but not always) all that we need. If you add a rule where regular expression capture groups can be mapped directly to fields, you can ignore `matchfunc` and the default function will suffice. Sometimes, however, it's necessary to do further processing, and so a rule can override `matchfunc` and assign its own processing function. Roman numerals are an obvious example where a regular expression capture group won't be sufficient to return a decimal integer directly.\n",
    "\n",
    "Regular expression patterns are matched with `re.VERBOSE` set, so the pattern can be split up across multiple lines, and each line can include a comment to help understand what it is doing. Comments alone don't always make clear nested structures in patterns, but they can help."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    id: rule_ur01\n",
      "  type: range\n",
      "fields: [earliest, latest]\n",
      "pattern:\n",
      "\n",
      "             ^\\[?         # optional [\n",
      "         (?:between\\s+)?  # optional \"between \"\n",
      "         (?:ca\\.\\s+)?     # optional \"ca. \"\n",
      "         (\\d{4})          # MATCH four digits\n",
      "         (?:-             # \"-\"\n",
      "         |                # or\n",
      "         \\s+and\\s+)       # \" and \"\n",
      "         (\\d{4})          # MATCH four digits\n",
      "         \\?               # \"?\"\n",
      "         [.\\]]*           # any # of . or ]\n",
      "         $\n"
     ]
    }
   ],
   "source": [
    "fieldlist = \", \".join(rule.fieldlist)\n",
    "print(f\"    id: {rule.ruleid}\\n  type: {rule.rtype}\\nfields: [{fieldlist}]\\npattern:\\n\")\n",
    "print(\"             \" + rule.pattern)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing\n",
    "\n",
    "There is a `catalog_year_test.py` file that can be run from the command line. It consists of a series of unit tests that call `exact_year` function with various examples in order to confirm that actual results match expected results in each case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "......................................\n",
      "----------------------------------------------------------------------\n",
      "Ran 38 tests in 0.009s\n",
      "\n",
      "OK\n"
     ]
    }
   ],
   "source": [
    "!python catalog_year_test.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There may be cases where the `catalog_year` code does not handle a date as it should. An ideal way to fix that would be to add a test that confirms and documents the error, then to add a processing rule that corrects the error, and finally to re-run the tests to confirm not only that that error is fixed, but that all previously tested cases continue to be OK after the fix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
