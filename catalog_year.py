#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

__version__ = '0.1.0'

import re

class DateVal:
    """A structure to return parsed date values"""
    def __init__(self, orig, datetype, ruleid = None):
        self.orig = orig
        self.datetype = datetype
        self.ruleid = ruleid
        self.year = None
        self.earliest = None
        self.latest = None

    def __repr__(self):
        if self.year:
            return "{} ({})".format(self.year, self.datetype)
        if self.earliest:
            return "{} - {}".format(self.earliest, self.latest)
        return "'{}'".format(self.orig)

class Rule:
    """A parsing rule intended to be part of a collection of rules; it consists
    primarily of a regular expression that captures matching groups. These 
    groups may either be mapped to a list of fields or transformed by a 
    custom function."""
    def __init__(self, ruleid, pattern, rtype, fieldlist = None):
        self.ruleid = ruleid
        self.pattern = pattern
        self.rtype = rtype
        self.fieldlist = fieldlist
        self.matchfunc = self._matchfields # the default matching function

    def _matchfields(self, val, groups):
        """Extends an object by matching the rule's regex pattern groups 
        to field names in the rule's fieldlist."""
        for fieldname, group in zip(self.fieldlist, groups):
            setattr(val, fieldname, group)
        return val

    def _trymatch(self, candidate):
        """Tests candidate against the pattern and returns a date value 
        if successful."""
        m = re.match(self.pattern, candidate, re.VERBOSE)
        if m:
            val = DateVal(candidate, self.rtype, self.ruleid)
            val = self.matchfunc(val, m.groups())
            return val
        return None

    def __repr__(self):
        return "{}".format(self.ruleid)

################################################################
#   
#  DATERULES are applied in order, and the first matching rule 
#  wins.
#
################################################################
    
DATERULES = []

################    UNCERTAIN RANGE

DATERULES.append(
    Rule("rule_ur01",
         r"""^\[?         # optional [
         (?:between\s+)?  # optional "between "
         (?:ca\.\s+)?     # optional "ca. "
         (\d{4})          # MATCH four digits
         (?:-             # "-"
         |                # or
         \s+and\s+)       # " and "
         (\d{4})          # MATCH four digits
         \?               # "?"
         [.\]]*           # any # of . or ]
         $""",
         "range",
         ["earliest","latest"]))

################    CIRCA

DATERULES.append(
    Rule("rule_c01",
         r""".*           # anything
         ca\.\s+          # "ca. "
         (\d{4})          # MATCH four digits
         \??              # optional ?
         \]*              # any # of ]
         $""",
         "circa",
         ["year"]))

################    UNCERTAIN 

DATERULES.append(
    Rule("rule_u01",
         r"""^(\d{4})     # MATCH four digits
         \?               # ?
         [.\]]*           # optional period or ]
         $""",
         "uncertain",
         ["year"]))



DATERULES.append(
    Rule("rule_u02",
         r"""^\D*         # any non-digits
         (?:\d{4})?       # four digits 
         \D*              # any non-digits
         \[               # optional [
         (?:i\.e\.\s      # "i.e. "
         |                # or
         or\s)?           # "or "
         (\d{4})          # MATCH four digits
         \?               # "?"
         \]               # ]
         \D{,3}       
         $""",
         "uncertain",
         ["year"]))

################    RANGE

DATERULES.append(
    Rule("rule_r01",
         r"""^\[?         # optional [
         (?:between\s+)?  # optional "between "
         (\d{4})          # MATCH four digits
         (?:-             # "-"
         |                # or
         \s+and\s+)       # " and "
         (\d{4})          # MATCH four digits
         [.\]]*           # any # of . or ]
         $""",
         "range",
         ["earliest","latest"]))

DATERULES.append(
    Rule("rule_r02",
         r"""^\D*         # any number of non-digits
         \[?
         (\d{4})          # MATCH four digits
         (?:\s+or\s+      # " or "
         |                # or
         /                # "/"
         |                # or
         -)               # "-"
         (\d{4})          # MATCH four digits
         \]*              # any number of ]
         $""",
         "range",
         ["earliest","latest"]))

def _match_optional_digit(val, groups):
    val.earliest = groups[0]
    val.latest = groups[0][:3] + groups[1]
    return val
r = Rule("match_optional_digit",
         r"""^\[?         # optional [
         (\d{4})          # MATCH four digits
         (?:\s+or\s+      # " or "
         |                # or 
         /                # "/"
         |                # or 
         -)               # "-"
         (\d)             # MATCH one digit
         \]?              # optional ]
         \.?              # optional .
         $""",
         "range")
r.matchfunc = _match_optional_digit
DATERULES.append(r)

def _match_optional_2digits(val, groups):
    val.earliest = groups[0]
    val.latest = groups[0][:2] + groups[1]
    return val
r = Rule("match_optional_2digits",
         r"""^\[?         # optional [
         ([0-9]{4})       # MATCH four digits
         (?:\s+or\s+      # " or "
         |                # or
         /                # "/"
         |                # or 
         -)               # "-"
         ([0-9]{2})       # MATCH two digits
         \]?              # optional ]
         \.?              # optional .
         $""",
         "range")
r.matchfunc = _match_optional_2digits
DATERULES.append(r)

def _match_uncertain_last_digit(val, groups):
    val.year = groups[0][:3] + groups[1]
    return val
r = Rule("match_uncertain_last_digit",
         r"""^(\d{3})     # MATCH three digits
         \[               # [
         (\d)             # MATCH one digit
         \??              # optional ?
         \]+              # one or more ]
         \.?              # optional period
         $""",
         "uncertain")
r.matchfunc = _match_uncertain_last_digit
DATERULES.append(r)

def _match_last_digit_frac45(val, groups):
    val.earliest = groups[0][:3] + "4"
    val.latest = groups[0][:3] + "5"
    return val
r = Rule("match_last_digit_frac45",
         r"""^(\d{3})     # MATCH three digits
         ⁴/₅              # ⁴/₅
         \D{,3}           # up to 3 non-digit chars
         $""",
         "range")
r.matchfunc = _match_last_digit_frac45
DATERULES.append(r)

def _match_last_digit_frac56(val, groups):
    val.earliest = groups[0][:3] + "5"
    val.latest = groups[0][:3] + "6"
    return val
r = Rule("match_last_digit_frac56",
         r"""^(\d{3})     # MATCH three digits
         ⁵/₆              # ⁵/₆
         \.?              # optional period
         $""",
         "range")
r.matchfunc = _match_last_digit_frac56
DATERULES.append(r)

def _match_decade(val, groups):
    val.earliest = groups[0][:3] + "0"
    val.latest = groups[0][:3] + "9"
    return val
r = Rule("match_decade",
         r"""^\D*         # any number of non-digits
         \[?              # optional [
         (\d{3})          # MATCH three digits
         \[?              # optional [
         (?:-             # "-"
         |                # or
         blank            # "blank"
         |                # or
         \?)?             # "?"
         \]?              # optional ]
         \D{,3}           # up to 3 non-digits
         $""",
         "range")
r.matchfunc = _match_decade
DATERULES.append(r)

def _match_century(val, groups):
    val.earliest = groups[0][:2] + "00"
    val.latest = groups[0][:2] + "99"
    return val
r = Rule("match_century",
         r"""^\[?       # optional [
         (\d{2})        # MATCH two digits
         [-?]{2}        # 2 of - or ?
         \??            # optional ?
         \]?            # optional ]
         $""",
         "range")
r.matchfunc = _match_century
DATERULES.append(r)

def _match_uncertain_range_2(val, groups):
    val.earliest = groups[0][:3] + groups[1]
    val.latest = groups[0][:3] + groups[2]
    return val
r = Rule("match_uncertain_range_2",
         r"""^
         (\d{3})        # MATCH three digits
         \[             # [
         (\d)           # MATCH one digit
         [- ]{1,2}      # "- "
         (\d)           # MATCH one digit
         \??            # optional ?
         \]?            # optional ]
         $""",
         "range")
r.matchfunc = _match_uncertain_range_2
DATERULES.append(r)

def _match_bracketed(val, groups):
    val.year = groups[0] + groups[1] + groups[2] + groups[3]
    if groups[4] == "?":
        val.datetype = "uncertain"
    return val
r = Rule("match_bracketed",
         r"""^\D*        # any number of non-digits
         \d{,2}          # up to 2 digits
         \D*             # any number of non-digits
         \[?             # optional [
         (\d)            # MATCH one digit
         \]?             # optional ]
         \[?             # optional [
         (\d)            # MATCH one digit
         \]?             # optional ]
         \[?             # optional [
         (\d)            # MATCH one digit
         \]?             # optional ]
         \[?             # optional [
         (\d)            # MATCH one digit
         \]?             # optional ]
         (\?)?           # OPTIONAL MATCH "?"
         .?              # optional .
         $""",
         "exact")
r.matchfunc = _match_bracketed
DATERULES.append(r)

def _match_sepdigits(val, groups):
    val.year = groups[0] + groups[1] + groups[2] + groups[3]
    if groups[4] == "?":
        val.datetype = "uncertain"
    return val
r = Rule("match_sepdigits",
         r"""^[^0-9]*    # any number of non-digits
         \[?             # optional [
         (\d)            # MATCH one digit
         \.?             # optional .
         (\d)            # MATCH one digit
         \.?             # optional .
         (\d)            # MATCH one digit
         \.?             # optional .
         (\d)            # MATCH one digit
         \.?             # optional .
         (\?)?           # OPTIONAL MATCH "?"
         .?              # optional .
         \D*         # any number of non-digits
         \d{,2}       # up to 2 digits
         \D*         # any number of non-digits
         $""",
         "exact")
r.matchfunc = _match_sepdigits
DATERULES.append(r)


################    EXACT

DATERULES.append(
    Rule("rule_x05",
         r"""^.*          # anything
         i\.e\.           # "i.e."
         .?\s?            # optional period, optional space
         (\d{4})          # MATCH four digits
         \]               # ]
         .?               # optional period
         $""",
         "exact",
         ["year"]))

DATERULES.append(
    Rule("rule_x02",
         r"""^\D*        # any number of non-digits
         (?:\d{,4}       # (up to 4 digits
         [^0-9\[\]]*)    # and any number of non-digits, without brackets)
         {,3}            # --up to 3x
         \[              # [
         (\d{4})         # MATCH four digits
         \]              # ]
         \D*             # any number of non-digits
         $""",    
         "exact",
         ["year"]))

DATERULES.append(
    Rule("rule_x03",
         r"""^\D*        # any number of non-digits
         \d{4}?          # four digits (or not)
         \D*             # any number of non-digits
         \d?             # maybe one digit
         \D*             # any number of non-digits
         \[              # [ 
         (?:\d{,2}       # up to 2 digits
         [^0-9\[\]]*)    # and any number of non-digits, without brackets
         (\d{4})         # MATCH four digits
         \]              # ]
         [^0-9?]{,3}     # up to three non-digits without ?
         $""",
         "exact",
         ["year"]))

DATERULES.append(Rule(
    "rule_x01", 
    r"""^\D*        # any number of non-digits
    (?:\d{,2}       # (up to two digits
    \D+){,3}        # followed by any number of non-digits) *-up to 3
    (\d{4})         # MATCH four digits
    [^0-9?]+        # any number of non-digits
    (?:\d{,2}       # up to two sequences of no more than 2 digits
    \D*){,2}        # in the context of any number of non-digits
    $""", #
    "exact", 
    ["year"]))

DATERULES.append(
    Rule(
        "rule_x04", 
        r"""^.*           # anything
         (\d{4})          # MATCH four digits
         \]               # ]
        $""", 
        "exact", 
        ["year"]))


################    ROMAN
     
def _roman_to_arabic(rstring):
    values = (("M", 1000), ("CM", 900), ("D", 500), ("CD", 400), 
              ("C", 100), ("XC", 90), ("L", 50), ("XL", 40), 
              ("X", 10), ("IX", 9), ("V", 5), ("IV", 4), ("I", 1))
    result = 0
    for (symbol, value) in values:
        while rstring.startswith(symbol):
            rstring = rstring[len(symbol):]
            result += value
    return result

def _match_roman(val, groups):
    roman = groups[0].replace(".", "").replace(" ", "").upper()
    val.year = str(_roman_to_arabic(roman))
    val.datetype = "exact"
    return val
r = Rule("match_roman",
         r"""^.*\b            # word boundary
         ([Mm]                # MATCH "M" and
         [DdCcLlXxVvIi. ]+)   # one or more roman numerals or spacing
         \b                   # followed by a word boundary
         .*                   # and anything else
         $""",
         "exact")
r.matchfunc = _match_roman
DATERULES.append(r)


################    NO MATCH

DATERULES.append(
    Rule("rule_n01",
         r"""^\D*$""",   # just non-digits
         "no_match",
         []))

def extract_year(date):
    """Return an annotated year dictionary given a date string 
    by applying the first matching rule in a list of DATERULES."""
    # an 'l' before three digits is probably a '1'
    date = re.sub("""l([0-9]{3})""", "1\\1", str(date).strip())
    for rule in DATERULES:
        try:
            val = rule._trymatch(date)
            if val:
                return val
        except:
            print("Error in {}".format(rule.ruleid))

    return DateVal(date, None)
